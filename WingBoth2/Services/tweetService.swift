//
//  tweetService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/30/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation
import Alamofire
import Firebase
class tweetService {
    static let instance = tweetService()
    
    struct GlobalVariable{
        static var itemLists = [Post]()
    }
    
    
    func getRandomHyperlocalPosts(radius: Int, timePeriod : Int, longitude : Double, latitude : Double, completion: @escaping (Array<Post>) -> Void) {
        var postArray = [Post]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                print(error)
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            
            let body: [String: Any] = [
                "Radius": radius,
                "TimePeriod": timePeriod,
                "Latitude": latitude,
                "Longitude": longitude,
                "Count" : 15 //can be larger than 15
            ]
            
             Alamofire.request(URL_ITEM_RANDOM_HYPERLOCAL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                    if((response.result.value) != nil) {
                        guard let json = response.result.value as? [String: Any] else {
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        let itemDict = info!["RandomItems"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let p = Post(title: itemsList[i]["ItemContent"] as! String, content: itemsList[i]["ItemContent"] as! String, date: strDate, itemId: itemsList[i]["ItemId"] as! String)
                                postArray.append(p)
                            }
                        }
                        completion(postArray)
                    }
            }
            
        }
        
    }
    
    func getRandomGlobalPosts(radius: Int, timePeriod : Int, longitude : Double, latitude : Double, completion: @escaping (Array<Post>) -> Void) {
        var postArray = [Post]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            
            let body: [String: Any] = [
                "Radius": radius,
                "TimePeriod": timePeriod,
                "Latitude": latitude,
                "Longitude": longitude,
                "Count" : 15 //can be larger than 15
            ]
            
            Alamofire.request(URL_ITEM_RANDOM_GLOBAL, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                print("tweet service getting random post response is \(response)")
                if((response.result.value) != nil) {
                    guard let json = response.result.value as? [String: Any] else {
                        print("didn't get todo object as JSON from API")
                        if let error = response.result.error {
                            print("Error: \(error)")
                        }
                        return
                    }
                    let info = json["Info"] as? [String : AnyObject]
                    let itemDict = info!["RandomItems"] as? [[String:Any]] //getting the RandomItems
                    //debugPrint(itemDict)
                    if let itemsList = itemDict {
                        for i in 0..<itemsList.count{
                            let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                            let p = Post(title: itemsList[i]["ItemContent"] as! String, content: itemsList[i]["ItemContent"] as! String, date: strDate, itemId: itemsList[i]["ItemId"] as! String)
                            postArray.append(p)
                        }

                    }
                    completion(postArray)
                    
                }
            }
            
        }
        
    }
    
    
    func postTweet(content: String, latitude : Double, longitude : Double, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "ItemContent": content,
                "ItemLatitude": latitude,
                "ItemLongitude": longitude
            ]
            Alamofire.request(URL_POST_TWEET, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }

    }
    
    
    func addComment(itemContent : String, itemId: String, commentContent: String, latitude : Double, longitude : Double, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "CommentContent": commentContent,
                "ItemContent" : itemContent,
                "CommentLatitude": latitude,
                "CommentLongitude": longitude
            ]
            let comment_add_url = URL_ITEM + itemId + "/c"
            Alamofire.request(comment_add_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    
    func getCommentsByPost(itemId : String, completion: @escaping (Array<Comment>) -> Void) {
        var commentsArray = [Comment]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let request_api = URL_ITEM + itemId + "/c"
            Alamofire.request(request_api, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                       // print("tweet service all comment info is \(info)")
                        let itemDict = info!["AllComments"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let c = Comment(comment: itemsList[i]["CommentContent"] as! String, date: strDate)
                                commentsArray.append(c)
                               
                            }
                            
                        }
                        completion(commentsArray)
                        
                    }
            }
            
        }
        
    }
    

}
