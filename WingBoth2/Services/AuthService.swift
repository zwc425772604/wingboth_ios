//
//  AuthService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/25/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation
import Firebase
import Alamofire
import SwiftKeychainWrapper



class AuthService {
    static let instance = AuthService()
    
    func registerUser(withEmail email: String, andPassword password: String, userCreationComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        Auth.auth().createUser(withEmail: email, password: password) { (user, error) in
            guard let user = user else {
                userCreationComplete(false, error)
                return
            }
            Auth.auth().currentUser?.sendEmailVerification { (error) in
                if error != nil{
                    print("failed to send")
                    userCreationComplete(false, error)
                    return 
                }else{
                    print("verification send successfully")
                }
            }
            
            userCreationComplete(true, nil)
        }
    }
    
    func loginUser(withEmail email: String, andPassword password: String, loginComplete: @escaping (_ status: Bool, _ error: Error?) -> ()) {
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                loginComplete(false, error)
                return
                //error alert
            }else{
                print("login success")
                
                let currentUser = Auth.auth().currentUser
                currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
                    if let error = error {
                        // Handle error
                        print(error)
                        loginComplete(false, nil)
                        return;
                    }
                }
                loginComplete(true, nil)
                
            }
        }
        
    }
    
    func signOutUser() -> Bool {
        let firebaseAuth = Auth.auth()
        do {
            try firebaseAuth.signOut()
        } catch let signOutError as NSError
        {
            print ("Error signing out: %@", signOutError)
            return false
        }
        return true
    }
    
    
}
