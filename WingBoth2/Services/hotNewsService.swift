//
//  hotNewsService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation
import Firebase
import Alamofire

class hotNewsService {
    
    static let instance = hotNewsService()
    
    func postHotItem(title : String, content: String, latitude : Double, longitude : Double, completeonClosure: @escaping (AnyObject?) -> ()) {
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "Title": title,
                "HotItemContent": content,
                "HotItemLatitude": latitude,
                "HotItemLongitude": longitude
            ]
            Alamofire.request(URL_HOT_ITEM_POST, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    
    
    
    func getRandomNewsAndComplaint(completion: @escaping (Array<HotItem>) -> Void) {
        var itemsArray = [HotItem]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                print("no sign in user \(error)")
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let count = 30
            let timeperiod = 2592000
            let random_url = URL_HOT_ITEM_RANDOM + "?count=" + String(count) +  "&timeperiod=" + String(timeperiod)
            Alamofire.request(random_url, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        let itemDict = info!["AllHotItemRandom"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let h = HotItem(title: itemsList[i]["HotItemTitle"] as! String, content: itemsList[i]["HotItemContent"] as! String, county: itemsList[i]["LocArea"] as! String, state: itemsList[i]["LocState"] as! String, country: itemsList[i]["LocCountry"] as! String, date: strDate, itemId:itemsList[i]["HotItemId"] as! String )
                                itemsArray.append(h)
                            }
                            
                        }
                        completion(itemsArray)
                        
                    }

                    completion(itemsArray)
                    
            }
            
        }
        
        
        
    }
    
    
    func getCommentsByHotItemId(itemId : String, completion: @escaping (Array<HotItemComment>) -> Void) {
        var commentsArray = [HotItemComment]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let request_api = URL_HOT + itemId + "/c"
            Alamofire.request(request_api, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        guard let json = response.result.value as? [String: Any] else {
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        let itemDict = info!["AllHotComments"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let h = HotItemComment(itemTitle : itemsList[i]["HotItemTitle"] as! String,comment: itemsList[i]["HotCommentContent"] as! String , date: strDate, itemId: itemsList[i]["HotItemId"] as! String)
                                commentsArray.append(h)
                                
                            }
                            
                        }
                        completion(commentsArray)
                        
                    }
            }
            
        }
    }
    
    
    
    func addComment(itemTitle : String, itemId: String, commentContent: String, latitude : Double, longitude : Double, itemStyle : String, completeonClosure: @escaping (AnyObject?) -> ()) {
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "HotCommentContent": commentContent,
                "HotItemTitle" : itemTitle,
                "HotCommentLatitude": latitude,
                "HotCommentLongitude": longitude,
                "HotItemStyle": itemStyle
            ]
            let comment_add_url = URL_HOT + itemId + "/c"
            Alamofire.request(comment_add_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    
    
    func getHotItemContent(itemId : String, completeClosure: @escaping (String) -> Void){
        let header = [
            "Content-Type": "application/json; charset=utf-8"
        ]
        let url_hot_item = URL_HOT + itemId
        Alamofire.request(url_hot_item, encoding: JSONEncoding.default, headers: header).responseJSON {
            response in
            if((response.result.value) != nil) {
                guard let json = response.result.value as? [String: Any] else {
                    if let error = response.result.error {
                        print("Error: \(error)")
                    }
                    return
                }
                let info = json["Info"] as? [String : AnyObject]
                let content = info!["HotItemContent"]
                completeClosure(content as! String)
            }
    }
    
    }
}
