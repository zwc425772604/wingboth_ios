//
//  profileService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/16/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation
import Alamofire
import Firebase

class profileService {
    static let instance = profileService()

    func userAddItem(content: String, latitude : Double, longitude : Double, itemId: String, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "ItemContent": content,
                "ItemLatitude": latitude,
                "ItemLongitude": longitude,
                "ItemId" : itemId
            ]
            let user_add_item_url = URL_USER_POST + "/" + itemId
            Alamofire.request(user_add_item_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }

    
    func userAddComment(itemContent : String, content: String, latitude : Double, longitude : Double, itemId: String,commentId : String, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "CommentContent": content,
                "CommentLatitude": latitude,
                "CommentLongitude": longitude,
                "ItemId" : itemId,
                "ItemContent": itemContent
            ]
            let user_add_comment_url = URL_USER_COMMENT + "/" + commentId
            Alamofire.request(user_add_comment_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    
    
    func userAddHotComment(itemTitle : String, content: String, latitude : Double, longitude : Double, itemId: String,commentId : String, itemStyle: String, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "HotCommentContent": content,
                "HotCommentLatitude": latitude,
                "HotCommentLongitude": longitude,
                "HotItemId" : itemId,
                "HotItemTitle": itemTitle,
                "HotItemStyle": itemStyle
            ]
            let user_add_comment_url = URL_USER_HOT_COMMENT + "/" + commentId
            Alamofire.request(user_add_comment_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    
    func getUserAllPosts(completion: @escaping (Array<Post>) -> Void) {
        var postArray = [Post]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            Alamofire.request(URL_UESR_ALL_POSTS, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        //let swiftyJsonVar = JSON(responseData.result.value!)
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        //print("profile service info is \(info)")
                        let itemDict = info!["AllPosts"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let p = Post(title: itemsList[i]["ItemContent"] as! String, content: itemsList[i]["ItemContent"] as! String, date: strDate, itemId: itemsList[i]["ItemId"] as! String)
                                postArray.append(p)
                            }

                        }
                        completion(postArray)
                      
                    }
            }
            
        }
     
       
        
    }
    
    func getUserAllComments(completion: @escaping (Array<profileComment>) -> Void) {
        var commentsArray = [profileComment]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            Alamofire.request(URL_USER_COMMENT, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        //let swiftyJsonVar = JSON(responseData.result.value!)
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        //print("profile service info is \(info)")
                        let itemDict = info!["AllComments"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let c = profileComment(postContent: itemsList[i]["ItemContent"] as! String, postId: itemsList[i]["ItemId"] as! String, comment: itemsList[i]["CommentContent"] as! String, date: strDate)
                                commentsArray.append(c)
                            }
                            
                        }
                        completion(commentsArray)
                        
                    }
            }
            
        }
        
        
        
    }
    
    
    
    func getUserAllHotComments(completion: @escaping (Array<HotItemComment>) -> Void) {
        var commentsArray = [HotItemComment]()
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            Alamofire.request(URL_USER_HOT_COMMENT, headers: headers)
                .responseJSON { response in
                    if((response.result.value) != nil) {
                        //let swiftyJsonVar = JSON(responseData.result.value!)
                        guard let json = response.result.value as? [String: Any] else {
                            print("didn't get todo object as JSON from API")
                            if let error = response.result.error {
                                print("Error: \(error)")
                            }
                            return
                        }
                        let info = json["Info"] as? [String : AnyObject]
                        //print("profile service info is \(info)")
                        let itemDict = info!["AllHotComments"] as? [[String:Any]] //getting the RandomItems
                        if let itemsList = itemDict {
                            for i in 0..<itemsList.count{
                                let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                                let h = HotItemComment(itemTitle: itemsList[i]["HotItemTitle"] as! String, comment: itemsList[i]["HotCommentContent"] as! String, date: strDate, itemId: itemsList[i]["HotItemId"] as! String)
                                commentsArray.append(h)
                            }
                            
                        }
                        completion(commentsArray)
                        
                    }
            }
            
        }
        
        
        
    }
    
    
    
    func getUserInfo(completeonClosure: @escaping (AnyObject?) -> ()) {
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
          
          
            Alamofire.request(URL_ADD_USER_MONGODB, headers: header)
                .responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
    }
    
    func userAddHotItem(title: String,content: String, latitude : Double, longitude : Double, itemId: String, completeonClosure: @escaping (AnyObject?) -> ()) {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        let currentUser = Auth.auth().currentUser   //can be refactored in later stage
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let header = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "ItemContent": content,
                "ItemLatitude": latitude,
                "ItemLongitude": longitude,
                "ItemId" : itemId
            ]
            let user_add_item_url = URL_USER_POST + "/" + itemId
            Alamofire.request(user_add_item_url, method: .post, parameters: body, encoding: JSONEncoding.default, headers: header).responseJSON {
                response in
                completeonClosure(response.result.value as AnyObject)
            }
        }
        
    }
    


}
