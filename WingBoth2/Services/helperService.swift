//
//  helperService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 9/13/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation

class helperService {
    static let instance = helperService()
    
    func convertTimeStampToDate(timestamp : Any) -> String {
        let date = Date(timeIntervalSince1970: timestamp as! TimeInterval)
        let dateFormatter = DateFormatter()
        dateFormatter.timeZone = TimeZone(abbreviation: "EST") //Set timezone that you want
        dateFormatter.locale = NSLocale.current
        dateFormatter.dateFormat = "yyyy-MM-dd HH:mm" //Specify your format that you want
        var strDate = dateFormatter.string(from: date)
        strDate += " EST"
        return strDate
        
    }
    
}
