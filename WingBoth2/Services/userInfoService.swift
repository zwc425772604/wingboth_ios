//
//  userInfoService.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/30/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation
import Firebase
import SwiftKeychainWrapper
import Alamofire

class userInfoService {
    static let instance = userInfoService()
    
    func getAccessToken() -> String {
        let accessToken : String? = KeychainWrapper.standard.string(forKey: "accessToken")

        return accessToken!
    }
    
    func getAllPosts() -> Array<Any>{
        var postArray = [Post]()
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        //print("userInfo token is \(accessToken)")
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
                "Content-Type": "application/json; charset=utf-8",
                "Authorization": idToken!
            ]
      
        Alamofire.request(URL_UESR_ALL_POSTS, headers: headers)
                                .responseJSON { response in
                                   //debugPrint(response)
                                   //get all the posts
                                   
                                    guard response.result.error == nil else {
                                        // got an error in getting the data, need to handle it
                                        print("error calling GET on /todos/1")
                                        print(response.result.error!)
                                        return
                                    }
                                    
                                    // make sure we got some JSON since that's what we expect
                                    guard let json = response.result.value as? [String: Any] else {
                                        print("didn't get todo object as JSON from API")
                                        if let error = response.result.error {
                                            print("Error: \(error)")
                                        }
                                        return
                                    }
                                    
                                    // get and print the title
                                    guard let info = json["Info"] as? NSDictionary else {
                                        print("Could not get todo title from JSON")
                                        return
                                    }
                                  
                                    
      
       
        }
       
        }
        let p = Post(title: "hello", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Ut enim ad minim veniam, quis nostrud exercitation ullamco laboris nisi ut aliquip ex ea commodo consequat. Duis aute irure dolor in reprehenderit in voluptate velit esse cillum dolore eu fugiat nulla pariatur. Excepteur sint occaecat cupidatat non proident, sunt in culpa qui officia deserunt mollit anim id est laborum.", date: "2018-07-22", itemId : "123241231")
        let d = Post(title: "testing", content: "Lorem ipsum dolor sit amet, consectetur adipiscing elit, sed do eiusmod tempor incididunt ut labore et dolore magna aliqua. Diam quis enim lobortis scelerisque fermentum dui. Tristique risus nec feugiat in.", date: "2018-07-24", itemId:"76443")
        let f = Post(title: "long title", content: "Hello it's me", date: "2018-08-02", itemId:"zzzzzzzzzz")
        let f1 = Post(title: "jdisoajd daoji", content: "Hello it's me from the other side", date: "2018-08-03", itemId:"987654321")
        postArray.append(p)
        postArray.append(d)
        postArray.append(f)
        postArray.append(f1)
        return postArray
        
    }
    
    
    func addUserToMongoDB(email: String) -> Bool {
        //let accessToken : String? = userInfoService.instance.getAccessToken()
        var success = false
        let currentUser = Auth.auth().currentUser
        currentUser?.getIDTokenForcingRefresh(true) { idToken, error in
            if let error = error {
                // Handle error
                return;
            }
            let headers = [
            "Content-Type": "application/json; charset=utf-8",
            "Authorization": idToken!
            ]
            let body: [String: Any] = [
                "email": email
            ]
            
            Alamofire.request(URL_ADD_USER_MONGODB, method: .post, parameters: body, encoding: JSONEncoding.default, headers: headers).responseJSON {
                response in
                //debugPrint(response)
                if response.result.isSuccess {
                    success = true
                }
          
            }
        }
        return success
    }
    
}
