//
//  profileVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/26/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class profileVC: UIViewController {
    static let instance = profileVC()
    
    var commentsArray = [profileComment]()
    
    @IBOutlet weak var emailLabel: UILabel!
    @IBOutlet weak var postCountLabel: UILabel!
    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var hotCommentCountLabel: UILabel!
    
    @IBOutlet weak var segmentedControll: UISegmentedControl! //posts or comment
    
   // @IBOutlet weak var tableView: UITableView!
    let progressHUD = progressBar(text: "Loading")
    
    struct GlobalVariable{
        static var postsArray = [Post]()
        static var commentsArray = [profileComment]()
        static var hotCommentsArray = [HotItemComment]()
    }
    
    var container: profileContainerVC!
 
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(progressHUD)
        segmentedControll.insertSegment(withTitle: "Hot Comments", at: 2, animated: true)
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updatePostCountLabel),
                                               name: NSNotification.Name(rawValue: "updatePostCount"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateCommentCountLabel),
                                               name: NSNotification.Name(rawValue: "updateCommentCount"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateHotCommentCountLabel),
                                               name: NSNotification.Name(rawValue: "updateHotCommentCount"),
                                               object: nil)
        getUserProfileInformation()
        
    }
    func getUserProfileInformation(){
        profileService.instance.getUserInfo(){
            response in
            let jsonString = self.jsonStringWithObject(obj: response!)
            let data = jsonString?.data(using: String.Encoding.utf8, allowLossyConversion: false)!
            do {
                let json = try JSONSerialization.jsonObject(with: data!, options: []) as! [String: AnyObject]
                let info = json["Info"] as? [String : AnyObject]
                let commentsDict = info!["AllComments"] as? [[String:Any]]
                let postsDict = info!["AllPosts"] as? [[String:Any]]
                let hotCommentsDict = info!["AllHotComments"] as? [[String:Any]]
                if let itemsList = commentsDict {
                     profileVC.GlobalVariable.commentsArray.removeAll()
                    for i in 0..<itemsList.count{
                        let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                        
                        let c = profileComment(postContent: itemsList[i]["ItemContent"] as! String, postId: itemsList[i]["ItemId"] as! String, comment: itemsList[i]["CommentContent"] as! String, date: strDate)
                        profileVC.GlobalVariable.commentsArray.append(c)
                    }
                    self.commentCountLabel.text = String(profileVC.GlobalVariable.commentsArray.count)
                    // NotificationCenter.default.post(name: Notification.Name("initCommentTable"), object: nil)
                    //NotificationCenter.default.post(Notification(name: Notification.Name(rawValue: "MessageReceived"),object: nil))
                }
                if let itemsList = postsDict {
                    profileVC.GlobalVariable.postsArray.removeAll()
                    for i in 0..<itemsList.count{
                        let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                        
                        let p = Post(title: itemsList[i]["ItemContent"] as! String, content: itemsList[i]["ItemContent"] as! String, date: strDate, itemId: itemsList[i]["ItemId"] as! String)
                        profileVC.GlobalVariable.postsArray.append(p)
                    }
                    postTableVC.GlobalVariable.postsArray = profileVC.GlobalVariable.postsArray
                    self.postCountLabel.text = String(profileVC.GlobalVariable.postsArray.count)
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "initPostTable"), object: nil, userInfo : nil)
                    
                }
                if let itemsList = hotCommentsDict {
                    profileVC.GlobalVariable.hotCommentsArray.removeAll()
                    for i in 0..<itemsList.count{
                        let strDate = helperService.instance.convertTimeStampToDate(timestamp: itemsList[i]["TimeStamp"]!)
                        
                        let h = HotItemComment(itemTitle: itemsList[i]["HotItemTitle"] as! String, comment: itemsList[i]["HotCommentContent"] as! String, date: strDate, itemId: itemsList[i]["HotItemId"] as! String)
                        profileVC.GlobalVariable.hotCommentsArray.append(h)
                    }
                    self.hotCommentCountLabel.text = String(profileVC.GlobalVariable.hotCommentsArray.count)
                    //NotificationCenter.default.post(name: NSNotification.Name(rawValue: "initHotCommentTable"), object: nil, userInfo : nil)
                }
            } catch let error as NSError {
                print("Failed to load: \(error.localizedDescription)")
            }
          self.progressHUD.removeFromSuperview()
            
        }

    }
    
    func jsonStringWithObject(obj: AnyObject) -> String? {
        var error: NSError?
        let jsonData  = try! JSONSerialization.data(withJSONObject: obj, options: JSONSerialization.WritingOptions(rawValue: 0))
        if error != nil {
            print("Error creating JSON data: \(error!.description)");
            return nil
        }
        return NSString(data: jsonData, encoding: String.Encoding.utf8.rawValue)! as String
    }

    @objc func updatePostCountLabel(_ notification: Notification) {
        
        if let data = notification.userInfo as? [String: Int]
        {
            for (_, score) in data
            {
                self.postCountLabel.text = String(score)
            }
        }
    }
    @objc func updateCommentCountLabel(_ notification: Notification) {
       
        if let data = notification.userInfo as? [String: Int]
        {
            for (_, score) in data
            {
                self.commentCountLabel.text = String(score)
            }
        }
    }
    @objc func updateHotCommentCountLabel(_ notification: Notification) {
        
        if let data = notification.userInfo as? [String: Int]
        {
            for (_, score) in data
            {
                self.hotCommentCountLabel.text = String(score)
            }
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileContainer"{
            self.container = segue.destination as! profileContainerVC
            //container.animationDurationWithOptions = (0.5, .transitionCrossDissolve)
            
        }
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(animated)
        self.postCountLabel.text = String(postTableVC.GlobalVariable.postCount)
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    @IBAction func logoutButtonPressed(_ sender: Any) {
        //logout code
        if AuthService.instance.signOutUser(){
            let authVC = storyboard?.instantiateViewController(withIdentifier: "mainPageVC")
            present(authVC!, animated: true, completion: nil)
        }
    }
  
   
    @IBAction func postOrCommentSegment(_ sender: Any) {
        switch segmentedControll.selectedSegmentIndex {
        case 0:
            container!.segueIdentifierReceivedFromParent("postSegue")
        case 1:
            container!.segueIdentifierReceivedFromParent("commentSegue")
        case 2:
            container!.segueIdentifierReceivedFromParent("hotCommentSegue")
        default:
            break
        }
    }
    
}
