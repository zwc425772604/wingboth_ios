//
//  postTableVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/15/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class postTableVC: UIViewController {

   
    @IBOutlet weak var postTable: UITableView!
    var postsArray = [Post]()
    
    struct GlobalVariable{
       static var postCount = 0
       static var selectedItemId = ""
       static var postsArray = [Post]()
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        print("set up post table view")
        
        
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(initPostTable),
                                               name: NSNotification.Name(rawValue: "initPostTable"),
                                               object: nil)
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadPostTable),
                                               name: NSNotification.Name(rawValue: "userAddPost"),
                                               object: nil)
        
        postTable.delegate = self
        postTable.dataSource = self
    }
    @objc func initPostTable(_ notification: Notification) {
        postsArray = profileVC.GlobalVariable.postsArray
        self.postTable.reloadData()
    }
    @objc func getUserAllPost(){
        profileService.instance.getUserAllPosts(){
            response in
            self.postsArray = response
            postTableVC.GlobalVariable.postCount = response.count
            self.postTable.reloadData()
            let postsCount = ["Count" : response.count]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updatePostCount"), object: nil, userInfo : postsCount)
            
        }
    }
    @objc func reloadPostTable(){
        getUserAllPost()
    }

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let postsCount = ["Count" : self.postsArray.count]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updatePostCount"), object: nil, userInfo : postsCount)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postDetailSegue"
        {
            let detailViewController = ((segue.destination) as! postDetailsVC)
            let indexPath = self.postTable!.indexPathForSelectedRow!
            let content = postsArray[indexPath.row].content
            detailViewController.postContent = content
        }
    }
}

extension postTableVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "postCell") as? PostCell else
        {return  UITableViewCell() }
        let post = postsArray[indexPath.row]
        cell.configureCell(content:post.content, date: post.date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
         homepageVC.GlobalVariable.selectedItemId = postsArray[indexPath.row].itemId
    }
}
