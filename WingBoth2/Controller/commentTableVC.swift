//
//  commentTableVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/15/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class commentTableVC: UIViewController {

    var commentsArray = [profileComment]()
    
    struct GlobalVariable{
        static var commentCount = 0
        static var selectedItemId = ""
        
    }
    
    @IBOutlet weak var commentTable: UITableView!
    override func viewDidLoad() {
        super.viewDidLoad()
        commentsArray = profileVC.GlobalVariable.commentsArray
        self.commentTable.reloadData()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadCommentTable),
                                               name: NSNotification.Name(rawValue: "userAddComment"),
                                               object: nil)
        commentTable.delegate = self
        commentTable.dataSource = self
       
    }
  
    
    @objc func initCommentTable(_ notification: Notification) {
        commentsArray = profileVC.GlobalVariable.commentsArray
        self.commentTable.reloadData()
    }
    @objc func getUserCommentPost(){
        profileService.instance.getUserAllComments(){
            response in
            self.commentsArray = response
            commentTableVC.GlobalVariable.commentCount = response.count
            self.commentTable.reloadData()
            let commentCount = ["Count" : response.count]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateCommentCount"), object: nil, userInfo : commentCount)
        }
    }
    
    @objc func reloadCommentTable(){
        getUserCommentPost()
    }

    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "commentPostDetailSegue"
        {
            let detailViewController = ((segue.destination) as! postDetailsVC)
            let indexPath = self.commentTable!.indexPathForSelectedRow!
            let content = commentsArray[indexPath.row].postContent
            detailViewController.postContent = content
        }
    }



}

extension commentTableVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "profileCommentCell") as? profileCommentCell else
        {return  UITableViewCell() }
        let comment = commentsArray[indexPath.row]
        cell.configureCell(postContent: comment.postContent, comment: comment.comment, date: comment.date)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        homepageVC.GlobalVariable.selectedItemId = commentsArray[indexPath.row].postId
        print("You tapped cell number \(indexPath.row).")
        print("selected item id is \(homepageVC.GlobalVariable.selectedItemId)")
        performSegue(withIdentifier: "commentPostDetailSegue", sender: nil)
    }
}
