//
//  AuthVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class AuthVC: UIViewController, GIDSignInUIDelegate  {

    @IBOutlet weak var googleSignInButton: GIDSignInButton!
    
    override func viewDidLoad() {
        super.viewDidLoad()
         GIDSignIn.sharedInstance().uiDelegate = self
        // Do any additional setup after loading the view.
    }
    
    
    @IBAction func signInWithGoogleButtonPressed(_ sender: Any) {
       
        GIDSignIn.sharedInstance().signIn()

    }
    
    @IBAction func signInWithEmailBtnWasPressed(_ sender: Any) {
        let loginVC = storyboard?.instantiateViewController(withIdentifier: "LoginVC")
        present(loginVC!, animated: true, completion: nil)
    }
    
    

   

}
