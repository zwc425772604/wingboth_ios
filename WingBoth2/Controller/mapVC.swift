//
//  mapVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/28/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation

var tappedLongitude : Double = 0.0
var tappedLatitude : Double = 0.0

class mapVC: UIViewController, UIGestureRecognizerDelegate,UISearchBarDelegate {

    @IBOutlet weak var mapView: MKMapView!
    
    var locationManager = CLLocationManager()
    let authorizationStatus = CLLocationManager.authorizationStatus()
    var regionRadius: Double = 1000 //this value need to get from user after they login in
    var userLocation: CLLocation? = nil
    

    override func viewDidLoad() {
        super.viewDidLoad()
        mapView.delegate = self
        locationManager.delegate = self
        locationManager.desiredAccuracy = kCLLocationAccuracyBest
        locationManager.requestWhenInUseAuthorization()
        locationManager.startUpdatingLocation()
        self.setMapview()
        centerMapOnUserLocation()
       // configureLocationServices()
    }
    func setMapview(){
        let lpgr = UILongPressGestureRecognizer(target: self, action: #selector(self.handleLongPress(gestureReconizer:)))
        lpgr.minimumPressDuration = 0.05
        lpgr.delaysTouchesBegan = true
        lpgr.delegate = self
        self.mapView.addGestureRecognizer(lpgr)
    }
    
    @objc func handleLongPress(gestureReconizer: UILongPressGestureRecognizer) {
        if gestureReconizer.state != UIGestureRecognizerState.ended {
            let touchLocation = gestureReconizer.location(in: self.mapView)
            let locationCoordinate = self.mapView.convert(touchLocation,toCoordinateFrom: self.mapView)
            addAnnotationOnLocation(pointedCoordinate: locationCoordinate)
            print("Tapped at lat: \(locationCoordinate.latitude) long: \(locationCoordinate.longitude)")
            tappedLatitude = locationCoordinate.latitude
            tappedLongitude = locationCoordinate.longitude
            return
        }
        if gestureReconizer.state != UIGestureRecognizerState.began {
            return
        }
    }

    @IBAction func viewPostButtonPressed(_ sender: Any) {
         self.updateRandomGlobalLocation()
    }
    
    func updateRandomGlobalLocation(){
        let postsCount = ["longitude" : tappedLongitude, "latitude" : tappedLatitude]
        NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateGlobalLocation"), object: nil, userInfo : postsCount)
        dismiss(animated: true, completion: nil)
    }
    
    func addAnnotationOnLocation(pointedCoordinate: CLLocationCoordinate2D) {
        let allAnnotations = self.mapView.annotations
        self.mapView.removeAnnotations(allAnnotations)
        
        let annotation = MKPointAnnotation()
        annotation.coordinate = pointedCoordinate
        annotation.title = "\(pointedCoordinate.latitude), \(pointedCoordinate.longitude)"
        annotation.subtitle = "Select this location?"
        mapView.addAnnotation(annotation)
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
 
        userLocation = locations[0]

    }
   
    @IBAction func backButtonPress(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    @IBAction func centerButtonPressed(_ sender: Any) {
        if authorizationStatus == .authorizedAlways || authorizationStatus == .authorizedWhenInUse {
            centerMapOnUserLocation()
        }
    }
    
    @IBAction func searchButtonPressed(_ sender: Any) {
        let searchController = UISearchController(searchResultsController: nil)
        searchController.searchBar.delegate = self
        present(searchController, animated: true, completion: nil)
    }
    
    func searchBarSearchButtonClicked(_ searchBar: UISearchBar)
    {
        //Ignoring user
        UIApplication.shared.beginIgnoringInteractionEvents()
        
        //Activity Indicator
        let activityIndicator = UIActivityIndicatorView()
        activityIndicator.activityIndicatorViewStyle = UIActivityIndicatorViewStyle.gray
        activityIndicator.center = self.view.center
        activityIndicator.hidesWhenStopped = true
        activityIndicator.startAnimating()
        
        self.view.addSubview(activityIndicator)
        
        //Hide search bar
        searchBar.resignFirstResponder()
        dismiss(animated: true, completion: nil)
        
        //Create the search request
        let searchRequest = MKLocalSearchRequest()
        searchRequest.naturalLanguageQuery = searchBar.text
        
        let activeSearch = MKLocalSearch(request: searchRequest)
        
        activeSearch.start { (response, error) in
            
            activityIndicator.stopAnimating()
            UIApplication.shared.endIgnoringInteractionEvents()
            
            if response == nil
            {
                print("ERROR")
            }
            else
            {
                //Remove annotations
                let annotations = self.mapView.annotations
                self.mapView.removeAnnotations(annotations)
                
                //Getting data
                let latitude = response?.boundingRegion.center.latitude
                let longitude = response?.boundingRegion.center.longitude
                
                tappedLatitude = latitude!
                tappedLongitude = longitude!
                
                //Create annotation
                let annotation = MKPointAnnotation()
                annotation.title = searchBar.text
                annotation.coordinate = CLLocationCoordinate2DMake(latitude!, longitude!)
                self.mapView.addAnnotation(annotation)
                
                //Zooming in on annotation
                let coordinate:CLLocationCoordinate2D = CLLocationCoordinate2DMake(latitude!, longitude!)
                let span = MKCoordinateSpanMake(0.1, 0.1)
                let region = MKCoordinateRegionMake(coordinate, span)
                self.mapView.setRegion(region, animated: true)
                
            }
            
        }
    }
    
    func  getUserLocation() -> (Double, Double){ // (latitude, longitude)
        return (self.userLocation!.coordinate.latitude, self.userLocation!.coordinate.longitude)
    }
    
}

extension mapVC : MKMapViewDelegate{
    func centerMapOnUserLocation() {
        print("center")
        guard let coordinate = locationManager.location?.coordinate else { return }
        print("latitude is \(coordinate.latitude) and longitude is \(coordinate.longitude)")
        let coordinateRegion = MKCoordinateRegionMakeWithDistance(coordinate, regionRadius * 2.0, regionRadius * 2.0)
        print("ok")
        mapView.setRegion(coordinateRegion, animated: true)
    }
  
}

extension mapVC : CLLocationManagerDelegate {
    func configureLocationServices(){
        if authorizationStatus == .notDetermined {
            locationManager.requestAlwaysAuthorization()
        }else{
            return
        }
    }

}
