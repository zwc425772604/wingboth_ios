//
//  hotNewsVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class hotNewsVC: UIViewController {

    @IBOutlet weak var hotItemsTableView: UITableView!
    var hotItemsArray = [HotItem]()
    
    let progressHUD = progressBar(text: "Loading")

    struct GlobalVariable{
        static var selectedItemId : String! = ""
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        self.view.addSubview(progressHUD)
        hotNewsService.instance.getRandomNewsAndComplaint(){
            response in
            self.hotItemsArray = response
            self.hotItemsTableView.reloadData()
            self.progressHUD.removeFromSuperview()
        }
        hotItemsTableView.estimatedRowHeight = 90.0
        hotItemsTableView.rowHeight = UITableViewAutomaticDimension
       
        hotItemsTableView.delegate = self
        hotItemsTableView.dataSource = self
       
    }
   

    @IBAction func postButtonPressed(_ sender: Any) {
        let popOverVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "postPopUp") as! addPostPopUpVC
        self.addChildViewController(popOverVC)
        popOverVC.view.frame = self.view.frame
        self.view.addSubview(popOverVC.view)
        popOverVC.didMove(toParentViewController: self)
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "hotDetailSegue"
        {
            let detailViewController = ((segue.destination) as! hotItemDetailVC)
            let indexPath = self.hotItemsTableView!.indexPathForSelectedRow!
            let content = hotItemsArray[indexPath.row].content
            let title = hotItemsArray[indexPath.row].title
            let date = hotItemsArray[indexPath.row].date
            let county = hotItemsArray[indexPath.row].county
            let state = hotItemsArray[indexPath.row].state
            let country = hotItemsArray[indexPath.row].country
            
            detailViewController.hotItemContent = content
            detailViewController.hotItemTitle = title
            detailViewController.hotItemDate = date
            detailViewController.hotItemCounty = county
            detailViewController.hotItemState = state
            detailViewController.hotItemCountry = country
        }
    }

    
}

extension hotNewsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotItemsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "HotItemCell") as? HotItemCell else
        {return  UITableViewCell() }
        let post = hotItemsArray[indexPath.row]
        cell.configureCell(title: post.title, county: post.county, state: post.state, country: post.country, date: post.date)
        return cell
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hotNewsVC.GlobalVariable.selectedItemId = hotItemsArray[indexPath.row].itemId
        performSegue(withIdentifier: "hotDetailSegue", sender: nil)
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return 90
    }
}
