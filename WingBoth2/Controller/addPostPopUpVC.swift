//
//  addPostPopUpVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class addPostPopUpVC: UIViewController {

    @IBOutlet weak var titleTextField: UITextField!
    @IBOutlet weak var contentTextView: UITextView!
    override func viewDidLoad() {
        super.viewDidLoad()
        
         self.view.backgroundColor = UIColor.black.withAlphaComponent(0.8)
         self.showAnimate()
        // Do any additional setup after loading the view.
    }

    @IBAction func submitButtonPressed(_ sender: Any) {

        if titleTextField.text != nil && contentTextView.text != nil {
            let title = self.titleTextField.text
            let content = self.contentTextView.text
            let coor = homepageVC.GlobalVariable.userLocation
            let latitude = coor?.coordinate.latitude
            let longitude = coor?.coordinate.longitude
            hotNewsService.instance.postHotItem(title: title!, content:content!, latitude: latitude!, longitude: longitude!){
                returnJSON in
                guard let json = returnJSON as? [String : AnyObject] else {
                    return
                }
                let info = json["Info"] as? [String : AnyObject]
                let itemId = info!["HotItemId"] as? String
                
             
                
            }
        }
        
        self.removeAnimate()
    }
    @IBAction func cancelButtonPressed(_ sender: Any) {
        self.removeAnimate()
    }
    
    func showAnimate()
    {
        self.view.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
        self.view.alpha = 0.0;
        UIView.animate(withDuration: 0.25, animations: {
            self.view.alpha = 1.0
            self.view.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        });
    }
    
    func removeAnimate()
    {
        UIView.animate(withDuration: 0.25, animations: {
            self.view.transform = CGAffineTransform(scaleX: 1.6, y: 1.6)
            self.view.alpha = 0.0;
        }, completion:{(finished : Bool)  in
            if (finished)
            {
                self.view.removeFromSuperview()
            }
        });
    }

}
