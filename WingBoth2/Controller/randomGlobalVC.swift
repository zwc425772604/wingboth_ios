//
//  randomGlobalVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/21/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

let MAX_BUFFER_SIZE = 3;
let  SEPERATOR_DISTANCE = 8;
let  TOPYAXIS = 75;


var longitude = homepageVC.GlobalVariable.userLocation?.coordinate.longitude
var latitude =  homepageVC.GlobalVariable.userLocation?.coordinate.latitude

import UIKit

class randomGlobalVC: UIViewController {

    @IBOutlet weak var postCardView: UIView!
 
   
    var currentIndex = 0
    var currentLoadedCardsArray = [postCard]()
    var allCardsArray = [postCard]()
    var valueArray = [String]()
    var randomPostsArray = [Post]()

    
    struct GlobalVariable{
        static var postsArray = [Post]()
    }
   
    override func viewDidLoad() {
        super.viewDidLoad()
        postCardView.alpha = 1
       

        NotificationCenter.default.addObserver(self,
                                               selector: #selector(updateRandomGlobal),
                                               name: NSNotification.Name(rawValue: "updateGlobalLocation"),
                                               object: nil)
        self.getRandomGlobalPost()
        let gesture = UITapGestureRecognizer(target: self, action:  #selector(self.viewPostDetail))
        self.postCardView.addGestureRecognizer(gesture)
       
    }
    
    @objc func updateRandomGlobal(_ notification: Notification) {
        if let data = notification.userInfo as? [String: Double]
        {
          longitude = data["longitude"]
          latitude = data["latitude"]
          self.getRandomGlobalPost()
        }
    }
    
    @objc func viewPostDetail(sender : UITapGestureRecognizer) {
        // Do what you want
        homepageVC.GlobalVariable.selectedItemId = self.randomPostsArray[currentIndex].itemId
        performSegue(withIdentifier: "postDetailSegue", sender: sender)
    }
    @IBAction func mapButtonPressed(_ sender: Any) {
        let mapVC = storyboard?.instantiateViewController(withIdentifier: "mapVC")
        present(mapVC!, animated: true, completion: nil)
    }
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        self.randomPostsArray.removeAll()
        self.valueArray.removeAll()
        self.currentLoadedCardsArray.removeAll()
        self.allCardsArray.removeAll()
        self.currentIndex = 0
        for view in self.postCardView.subviews {
            view.removeFromSuperview()
        }
        getRandomGlobalPost()
    }
    func getRandomGlobalPost(){
        let progressHUD = progressBar(text: "Loading")
        self.view.addSubview(progressHUD)
        self.randomPostsArray.removeAll()
        self.valueArray.removeAll()
        self.currentLoadedCardsArray.removeAll()
        self.allCardsArray.removeAll()
        self.currentIndex = 0
        
        let distanceFilter = 8000
        let timeFilter = 2592000
        
        tweetService.instance.getRandomGlobalPosts(radius: distanceFilter, timePeriod: timeFilter, longitude: longitude!, latitude: latitude!){
            response in
            self.randomPostsArray = response
            if self.randomPostsArray.count == 0 {
                 self.postCardView.alpha = 0
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindowLevelAlert + 1
                
                let alertController = UIAlertController(title: "Oops!", message: "No post found within 2 miles in this region. Please select another region.", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                progressHUD.removeFromSuperview()
            }
            else{
                GlobalVariable.postsArray = self.randomPostsArray
                self.postCardView.alpha = 1
                for i in 0..<self.randomPostsArray.count{
                    self.valueArray.append(String(i+1))
                }
                self.loadCardValues()
                progressHUD.removeFromSuperview()
                
            }
           
        }
        
    }
    
    override func viewWillAppear(_ animated: Bool) {
        super.viewWillAppear(true)
        view.layoutIfNeeded()

    }
    func loadCardValues() {
        self.currentIndex  = 0
        if valueArray.count > 0 {
            
            let capCount = (valueArray.count > MAX_BUFFER_SIZE) ? MAX_BUFFER_SIZE : valueArray.count
            
            for (i,value) in valueArray.enumerated() {
                let newCard = createTinderCard(at: i,value: value)
                allCardsArray.append(newCard)
                if i < capCount {
                    currentLoadedCardsArray.append(newCard)
                }
            }
            
            for (i,_) in currentLoadedCardsArray.enumerated() {
                if i > 0 {
                    self.postCardView.insertSubview(currentLoadedCardsArray[i], belowSubview: currentLoadedCardsArray[i - 1])
                }else {
                    self.postCardView.addSubview(currentLoadedCardsArray[i])
                }
            }
            animateCardAfterSwiping()
            perform(#selector(loadInitialDummyAnimation), with: nil, afterDelay: 1.0)
        }
    }
    
    @objc func loadInitialDummyAnimation() {
        
        //let dummyCard = currentLoadedCardsArray.first;
        //dummyCard?.shakeAnimationCard()
        UIView.animate(withDuration: 1.0, delay: 2.0, options: .curveLinear, animations: {
            self.postCardView.alpha = 1.0
        }, completion: nil)
      
    }
    
    func createTinderCard(at index: Int , value :String) -> postCard {
        
        let card = postCard(frame: CGRect(x: 0, y: 0, width: postCardView.frame.size.width , height: postCardView.frame.size.height) ,value : value, index: index)
        card.delegate = self as? postCardDelegate
        return card
    }
    
    func removeObjectAndAddNewValues() {
        currentLoadedCardsArray.remove(at: 0)
        currentIndex = currentIndex + 1
        if (currentIndex + currentLoadedCardsArray.count) < allCardsArray.count {
            let card = allCardsArray[currentIndex + currentLoadedCardsArray.count]
            var frame = card.frame
            frame.origin.y = CGFloat(MAX_BUFFER_SIZE * SEPERATOR_DISTANCE)
            card.frame = frame
            currentLoadedCardsArray.append(card)
            self.postCardView.insertSubview(currentLoadedCardsArray[MAX_BUFFER_SIZE - 1], belowSubview: currentLoadedCardsArray[MAX_BUFFER_SIZE - 2])
        }
        animateCardAfterSwiping()
        if currentLoadedCardsArray.count == 0{
            getRandomGlobalPost()
        }
    }
    
    func animateCardAfterSwiping() {
        
        for (i,card) in currentLoadedCardsArray.enumerated() {
            UIView.animate(withDuration: 0.5, animations: {
                if i == 0 {
                    card.isUserInteractionEnabled = true
                }
                var frame = card.frame
                frame.origin.y = CGFloat(i * SEPERATOR_DISTANCE)
                card.frame = frame
            })
        }
    }
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postDetailSegue"
        {
            let detailViewController = ((segue.destination) as! postDetailsVC)
            let content = self.randomPostsArray[currentIndex].content
            //detailViewController.postContent = content
            detailViewController.postContent = content
        }
    }
}
extension randomGlobalVC : postCardDelegate{
    func currentCardStatus(card: postCard, distance: CGFloat) {
        if distance == 0 {
           
        }else{
           
        }
    }
    
    
    // action called when the card goes to the left.
    func cardGoesLeft(card: postCard) {
        removeObjectAndAddNewValues()
    }
    // action called when the card goes to the right.
    func cardGoesRight(card: postCard) {
        removeObjectAndAddNewValues()
    }
   
}
