//
//  mainPageVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 9/6/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import Firebase
import GoogleSignIn

class mainPageVC: UIViewController, GIDSignInUIDelegate {

   
    @IBOutlet weak var emailTextField: UITextField!
    @IBOutlet weak var passwordTextField: UITextField!
    
    var rootVC : UIViewController?
   
    override func viewDidLoad() {
        super.viewDidLoad()
        GIDSignIn.sharedInstance().uiDelegate = self
        
        let tap = UITapGestureRecognizer(target: self, action: #selector(mainPageVC.handleTap))
        view.addGestureRecognizer(tap)
    }
    
    @objc func handleTap() {
        view.endEditing(true)
    }

    
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard
            let email = emailTextField.text,
            let password = passwordTextField.text,
            email.count > 0,
            password.count > 0
            else {
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindowLevelAlert + 1
                
                let alertController = UIAlertController(title: "Login Failure", message: "Invalid email or password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                
            }else{
                
                
                let currentUser = Auth.auth().currentUser
                if !(currentUser?.isEmailVerified)!{
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1
                    
                    let alertController = UIAlertController(title: "Email not verified", message: "Do you want us to send another verification email to \(email)", preferredStyle: .alert)
                    alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                    let alertActionOkay = UIAlertAction(title: "Yes", style: .default) {
                        (_) in
                        currentUser?.sendEmailVerification(completion: nil)
                    }
                    alertController.addAction(alertActionOkay)
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                    
            
                }else
                {
                    self.rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! UITabBarController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = self.rootVC
                }
                
                
            }
        }
    
        
    }
    
    @IBAction func forgetPasswordButtonPressed(_ sender: Any) {
        let alertController = UIAlertController(title: "Reset Password", message: "Enter your email and we will send you instruction on how to reset it", preferredStyle: .alert)
        
        //the confirm action taking the inputs
        let confirmAction = UIAlertAction(title: "Send Now", style: .default) { (_) in
            
            //getting the input values from user
            let email = alertController.textFields?[0].text
            Auth.auth().sendPasswordReset(withEmail: email!) { (error) in
                
            }
        }
        alertController.addTextField { (textField) in
            textField.placeholder = "Enter your email"
        }
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel) { (_) in }
        alertController.addAction(confirmAction)
        alertController.addAction(cancelAction)
        
        //finally presenting the dialog box
        self.present(alertController, animated: true, completion: nil)
    }
    
    @IBAction func loginWithGooglePressed(_ sender: Any) {
         GIDSignIn.sharedInstance().signIn()
    }
    
    @IBAction func loginWithFacebookPressed(_ sender: Any) {
    }
    
    @IBAction func createAccountPressed(_ sender: Any) {
        let createAccountVC = storyboard?.instantiateViewController(withIdentifier: "createAccountVC")
        present(createAccountVC!, animated: true, completion: nil)
    }
}
