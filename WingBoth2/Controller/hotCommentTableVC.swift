//
//  hotCommentTableVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/29/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class hotCommentTableVC: UIViewController {
    var hotCommentsArray = [HotItemComment]()
    
    
    @IBOutlet weak var hotCommentTableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    struct GlobalVariable{
        static var commentCount = 0
        static var selectedItemId = ""
        
    }
    override func viewDidLoad() {
        super.viewDidLoad()
        hotCommentsArray = profileVC.GlobalVariable.hotCommentsArray
        self.hotCommentTableView.reloadData()
        NotificationCenter.default.addObserver(self,
                                               selector: #selector(reloadCommentTable),
                                               name: NSNotification.Name(rawValue: "userAddHotComment"),
                                               object: nil)
    
        hotCommentTableView.delegate = self
        hotCommentTableView.dataSource = self

    }
    @objc func initHotCommentTable(_ notification: Notification) {
        hotCommentsArray = profileVC.GlobalVariable.hotCommentsArray
        self.hotCommentTableView.reloadData()
    }
    
    @objc func getUserHotCommentPost(){
        profileService.instance.getUserAllHotComments(){
            response in
            self.hotCommentsArray = response
            hotCommentTableVC.GlobalVariable.commentCount = response.count
            self.hotCommentTableView.reloadData()
            let commentCount = ["Count" : response.count]
            NotificationCenter.default.post(name: NSNotification.Name(rawValue: "updateHotCommentCount"), object: nil, userInfo : commentCount)
        }
    }
    
    @objc func reloadCommentTable(){
        getUserHotCommentPost()
    }
    
    
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "profileHotItemDetailSegue"
        {
            let detailViewController = ((segue.destination) as! hotItemDetailVC)
            let indexPath = self.hotCommentTableView!.indexPathForSelectedRow!
            
            //getting the hot item content
            let itemId = hotCommentsArray[indexPath.row].itemId
            var content = ""
            hotNewsService.instance.getHotItemContent(itemId: itemId){
                response in
                content = response
            }
            let title = hotCommentsArray[indexPath.row].itemTitle
            let date = hotCommentsArray[indexPath.row].date
            detailViewController.hotItemContent = content
            detailViewController.hotItemTitle = title
            detailViewController.hotItemDate = date
        }
    }

   
}


extension hotCommentTableVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return hotCommentsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "profileHotCommentCell") as? profileHotCommentCell else
        {return  UITableViewCell() }
        let comment = hotCommentsArray[indexPath.row]
        cell.configureCell(itemTitle: comment.itemTitle, comment: comment.comment, date: comment.date)
        return cell
    }
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        hotNewsVC.GlobalVariable.selectedItemId = hotCommentsArray[indexPath.row].itemId
        performSegue(withIdentifier: "profileHotItemDetailSegue", sender: nil)
    }
}
