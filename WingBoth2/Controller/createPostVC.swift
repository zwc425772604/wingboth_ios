//
//  createPostVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/27/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class createPostVC: UIViewController,UITextViewDelegate {

    
    @IBOutlet weak var contentTextView: UITextView!
    
    var homepageViewController = homepageVC()
    
    override func viewDidLoad() {
        super.viewDidLoad()

        let tap = UITapGestureRecognizer(target: self, action: #selector(createPostVC.handleTap))
        view.addGestureRecognizer(tap)
        
        contentTextView.delegate = self
        contentTextView.text = "What is in your mind?"
        contentTextView.textColor = UIColor.lightGray
       
    }
    
    @objc func handleTap() {
        
        view.endEditing(true)
    }
    
    func textViewDidBeginEditing(_ textView: UITextView) {
        if contentTextView.textColor == UIColor.lightGray {
            contentTextView.text = ""
            contentTextView.textColor = UIColor.black
        }
    }
    
    func textViewDidEndEditing(_ textView: UITextView) {
        if contentTextView.text == "" {
            contentTextView.text = "What is in your mind?"
            contentTextView.textColor = UIColor.lightGray
        }
    }
    
    func textViewDidChange(_ textView: UITextView) {
        if contentTextView.text == ""{
            contentTextView.text = "What is in your mind?"
            contentTextView.textColor = UIColor.lightGray
            contentTextView.resignFirstResponder()
        }
    }
    
    @IBAction func cancelButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func postButtonPressed(_ sender: Any) {
         if contentTextView.text != nil {

            let progressHUD = progressBar(text: "Saving post")
            self.view.addSubview(progressHUD)
            
            let content = self.contentTextView.text
            let coor = homepageVC.GlobalVariable.userLocation
            let latitude = coor?.coordinate.latitude
            let longitude = coor?.coordinate.longitude
            tweetService.instance.postTweet(content:content!, latitude: latitude!, longitude: longitude!){
                returnJSON in
                guard let json = returnJSON as? [String : AnyObject] else {
                    return
                }
                let info = json["Info"] as? [String : AnyObject]
                let itemId = info!["ItemId"] as? String
                
                //add the item to allUser collection
                profileService.instance.userAddItem(content: content!, latitude: latitude!, longitude: longitude!, itemId: itemId!)
                {
                    returnJSON in
                    //notify profile vc to reload the post table
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userAddPost"), object: nil, userInfo : nil)
                    guard let json = returnJSON as? [String : AnyObject] else {
                        return
                    }
                    let status = json["Status"] as? String
                    if status == "OK" {
                        progressHUD.removeFromSuperview()
                        let progressStatus = progressBar(text: "SUCCESS")
                        self.view.addSubview(progressStatus)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            progressStatus.removeFromSuperview()
                        })
                    }else{
                        progressHUD.removeFromSuperview()
                        let progressStatus = progressBar(text: "FAILED")
                        self.view.addSubview(progressStatus)
                        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
                            progressStatus.removeFromSuperview()
                        })
                    }
                }
            }
        }
        self.contentTextView.text = ""
        self.contentTextView.resignFirstResponder()
    }
    
}



