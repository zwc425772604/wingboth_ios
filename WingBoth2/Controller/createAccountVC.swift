//
//  createAccountVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/25/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class createAccountVC: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

    @IBAction func closeButtonPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
    @IBAction func signUpButtonPressed(_ sender: Any) {
        if emailField.text != nil && passwordField.text != nil {
//            let spinner: UIActivityIndicatorView = UIActivityIndicatorView(frame: CGRect(x: 0, y: 0, width: 100, height: 100)) as UIActivityIndicatorView
//            spinner.startAnimating()
            AuthService.instance.registerUser(withEmail: self.emailField.text!, andPassword: self.passwordField.text!, userCreationComplete: { (success, registrationError) in
                
//                spinner.stopAnimating()
                    if success {
                        AuthService.instance.loginUser(withEmail: self.emailField.text!, andPassword: self.passwordField.text!, loginComplete: { (success, nil) in
                            //send add user request to profileService
                            userInfoService.instance.addUserToMongoDB(email: self.emailField.text!)
                            
                            let alertController = UIAlertController(title: "Success", message: "Please verify your account in your email.", preferredStyle: .alert)
                            alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                            
                            self.present(alertController, animated: true, completion: nil)
                       
                            print("Successfully registered user")
                            AuthService.instance.signOutUser()
                            
                            let authVC = self.storyboard?.instantiateViewController(withIdentifier: "mainPageVC")
                            self.present(authVC!, animated: true, completion: nil)
                            //go back to login page and display the message that the user must verify his/her account
                        })
                    } else {
                        let alertController = UIAlertController(title: "Failure", message: "Please provide correct email address.", preferredStyle: .alert)
                         alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(alertController, animated: true, completion: nil)
                        print(String(describing: registrationError?.localizedDescription))
                    }
                })
        }
    }
        
    
    
}
