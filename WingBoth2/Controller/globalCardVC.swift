//
//  globalCardVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/14/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class globalCardVC: UIViewController {

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var cardView: UIView!
    
    var divisionParam: CGFloat!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        divisionParam = (view.frame.size.width/2)/0.61

        // Do any additional setup after loading the view.
    }

    @IBAction func panGestureValueChanged(_ sender: UIPanGestureRecognizer) {
        let cardView = sender.view!
        let translationPoint = sender.translation(in: view)
        cardView.center = CGPoint(x: view.center.x+translationPoint.x, y: view.center.y+translationPoint.y)
        
        let distanceMoved = cardView.center.x - view.center.x
        if distanceMoved > 0 { // moved right side
           print("move to right")
        }
        else { // moved left side
           print("move to left")
        }
        
        //Tilt your card
        cardView.transform = CGAffineTransform(rotationAngle: distanceMoved/divisionParam)
        
        if sender.state == UIGestureRecognizerState.ended {
            if cardView.center.x < 20 { // Moved to left
                UIView.animate(withDuration: 0.3, animations: {
                    cardView.center = CGPoint(x: cardView.center.x-200, y: cardView.center.y)
                })
                return
            }
            else if (cardView.center.x > (view.frame.size.width-20)) { // Moved to right
                UIView.animate(withDuration: 0.3, animations: {
                    cardView.center = CGPoint(x: cardView.center.x+200, y: cardView.center.y)
                })
                return
            }
            
            UIView.animate(withDuration: 0.2, animations: {
                self.resetCardViewToOriginalPosition()
            })
        }
    }
    func resetCardViewToOriginalPosition(){
        cardView.center = self.view.center
        //self.contentLabel.alpha = 0
        cardView.transform = .identity
    }

}
