//
//  LoginVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import Firebase

class LoginVC: UIViewController {

    @IBOutlet weak var emailField: UITextField!
    @IBOutlet weak var passwordField: UITextField!
    
     var rootVC : UIViewController?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        // Do any additional setup after loading the view.
    }

  
    @IBAction func closePressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    @IBAction func loginButtonPressed(_ sender: Any) {
        guard
            let email = emailField.text,
            let password = passwordField.text,
            email.count > 0,
            password.count > 0
            else {
                return
        }
        
        Auth.auth().signIn(withEmail: email, password: password) { user, error in
            if let error = error, user == nil {
                print("login error login vc is \(error)")
                 AuthService.instance.signOutUser()
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindowLevelAlert + 1
                
                let alertController = UIAlertController(title: "Login Failure", message: "Invalid email or password", preferredStyle: .alert)
                alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
                
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                
                //UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)
               
            }else{
                
                
                let currentUser = Auth.auth().currentUser
                if !(currentUser?.isEmailVerified)!{
                    print("email not verified")
                    
                    let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                    alertWindow.rootViewController = UIViewController()
                    alertWindow.windowLevel = UIWindowLevelAlert + 1
                    
                    let alertController = UIAlertController(title: "Email is not verified", message: "Do you want us to send another verification email to \(email)", preferredStyle: .alert)
                   // alertController.addAction(UIAlertAction(title: "No", style: .default, handler: nil))
                    
                    
                    let alertActionNo = UIAlertAction(title: "No", style: .default) {
                        (_) in
                        print("no, sign out user")
                        AuthService.instance.signOutUser()
                    }
                    
                    let alertActionOkay = UIAlertAction(title: "Yes", style: .default) {
                        (_) in
                        currentUser?.sendEmailVerification(completion: nil)
                        print("yes sign out user")
                        AuthService.instance.signOutUser()
                    }
                   
                    alertController.addAction(alertActionOkay)
                    alertController.addAction(alertActionNo)
                    alertWindow.makeKeyAndVisible()
                    alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
                    
//                    let alertController = UIAlertController(title: "Email not verified", message: "Please verify your account.", preferredStyle: .alert)
//                    alertController.addAction(UIAlertAction(title: "Dismiss", style: .default, handler: nil))
//                    UIApplication.shared.keyWindow?.rootViewController?.present(alertController, animated: true, completion: nil)                    //self.present(alertController, animated: true, completion: nil)
                }else
                {
                    print("login user in loginVC")
                    self.rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! UITabBarController
                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
                    appDelegate.window?.rootViewController = self.rootVC
                    print("jump")
                }
            
                
            }
        }
        
//        AuthService.instance.loginUser(withEmail: self.emailField.text!, andPassword: self.passwordField.text!, loginComplete: { (success, loginError) in
//            if success {
//                    //self.dismiss(animated: true, completion: nil)
//                    //go to main page
//                    print("login user in loginVC")
//                    self.rootVC = UIStoryboard(name: "Main", bundle: nil).instantiateViewController(withIdentifier: "homeVC") as! UITabBarController
//                    let appDelegate = UIApplication.shared.delegate as! AppDelegate
//                    appDelegate.window?.rootViewController = self.rootVC
//                    print("jump")
//            } else {
//                print(String(describing: loginError?.localizedDescription))
//            }
//        })
        

    }
    
    @IBAction func signUpPressed(_ sender: Any) {
        let createAccountVC = storyboard?.instantiateViewController(withIdentifier: "createAccountVC")
        present(createAccountVC!, animated: true, completion: nil)
    }
    
}
