//
//  postDetailsVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/8/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit


class postDetailsVC: UIViewController {
    var postContent: String! = ""
    var commentsArray = [Comment]()
    let progressHUD = progressBar(text: "Loading")
    
    @IBOutlet weak var commentsTableView: UITableView!
    
    @IBOutlet weak var contentTextView: UITextView!
    
    @IBOutlet weak var commentTextField: UITextField!
    
    override func viewDidLoad() {
        super.viewDidLoad()
        view.bindToKeyboard()
        self.view.addSubview(progressHUD)
        let tap = UITapGestureRecognizer(target: self, action: #selector(postDetailsVC.handleTap))
        view.addGestureRecognizer(tap)
        
        contentTextView.text = postContent
        commentsTableView.delegate = self
        commentsTableView.dataSource = self

    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let itemId = homepageVC.GlobalVariable.selectedItemId
        tweetService.instance.getCommentsByPost(itemId: itemId){
            response in
            print(response)
            self.commentsArray = response
            self.commentsTableView.reloadData()
            self.progressHUD.removeFromSuperview()
        }
        
    }
   
    @IBAction func commentButtonPressed(_ sender: Any) {
        //print("comment button pressed")
        let comment = commentTextField.text
        if comment == "" {
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1
            
            let alertController = UIAlertController(title: "Comment", message: "Comment is empty. Please enter your comment.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }else{
            
            let coor = homepageVC.GlobalVariable.userLocation
            let latitude = coor?.coordinate.latitude
            let longitude = coor?.coordinate.longitude
            let content = self.contentTextView.text
            let itemId = homepageVC.GlobalVariable.selectedItemId
        
            tweetService.instance.addComment(itemContent: content!, itemId: itemId, commentContent: comment!, latitude: latitude!, longitude: longitude!){
                returnJSON in
                guard let json = returnJSON as? [String : AnyObject] else {
                    return
                }
                let info = json["Info"] as? [String : AnyObject]
                let commentId = info!["CommentId"] as? String
                
                //add the item to allUser collection
                profileService.instance.userAddComment(itemContent: content!, content: comment!, latitude: latitude!, longitude: longitude!, itemId: itemId, commentId: commentId!){
                    returnJSON in
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userAddComment"), object: nil, userInfo : nil)
                }
                
            }
            self.commentTextField.text = ""
            self.commentTextField.resignFirstResponder()
        }
    }
    @objc func handleTap() {
        
        view.endEditing(true)
    }

    @IBAction func hahaPressed(_ sender: Any) {
        dismiss(animated: true, completion: nil)
    }
    
}

extension postDetailsVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //mainFeedCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "commentCell") as? CommentCell else
        {return  UITableViewCell() }
        let comment = commentsArray[indexPath.row]
        cell.configureCell(comment:comment.comment, commentNumber: indexPath.row + 1)
        cell.setNeedsLayout() //invalidate current layout
        cell.layoutIfNeeded() //update immediately
        cell.setNeedsDisplay()
        cell.setSelected(true, animated: true)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
        
    }
   
    
    
}
