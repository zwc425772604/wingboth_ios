//
//  homepageVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/26/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import MapKit
import CoreLocation
import Alamofire


class homepageVC: UIViewController,CLLocationManagerDelegate, MKMapViewDelegate,UIPickerViewDataSource, UIPickerViewDelegate{
  
    @IBOutlet weak var distanceDropdown: UIPickerView!
    let timeArray:[String] = ["Past Week", "Past Month", "Past 3 Months", "Anytime"]
    let distanceArray:[String] = ["Within 2 miles", "Within 5 miles", "Within 10 miles", "Current Zip Code Area"]
    
    var timeFilter : Int = 2592000 //express in seconds
    var distanceFilter: Int = 8000 //express in meters

    @IBOutlet weak var filterTextField: UITextField!
    @IBOutlet weak var mainFeedTableView: UITableView!
    var postsArray = [Post]()
    var locationManager: CLLocationManager = CLLocationManager()
    
    let cellSpacingHeight : CGFloat = 5
    
    
    struct GlobalVariable{
        static var userLocation: CLLocation? = nil
        static var selectedItemId = ""
    }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        distanceDropdown.delegate = self
        distanceDropdown.dataSource = self
        distanceDropdown.reloadAllComponents()
        
        self.locationManager.requestWhenInUseAuthorization()
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.desiredAccuracy = kCLLocationAccuracyNearestTenMeters
            locationManager.startUpdatingLocation()
        }
        
        DispatchQueue.main.asyncAfter(deadline: .now() + 1, execute: {
            self.getRandomPost()
        })
        
        mainFeedTableView.delegate = self
        mainFeedTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
    }
    
    @IBAction func refreshButtonPressed(_ sender: Any) {
        getRandomPost()
    }
    
    func scrollToFirstRow() {
        let indexPath = IndexPath(row: 0, section: 0)
        self.mainFeedTableView.scrollToRow(at: indexPath, at: .top, animated: true)
    }
    
    @objc func getRandomPost(){
        let progressHUD = progressBar(text: "Loading")
        self.view.addSubview(progressHUD)
        self.postsArray.removeAll()
        let longitude = GlobalVariable.userLocation?.coordinate.longitude
        let latitude = GlobalVariable.userLocation?.coordinate.latitude
        tweetService.instance.getRandomHyperlocalPosts(radius: distanceFilter, timePeriod: timeFilter, longitude: longitude!, latitude: latitude!){
            response in
            self.postsArray = response
            self.mainFeedTableView.reloadData()
            progressHUD.removeFromSuperview()
            self.scrollToFirstRow()
        }        
    }
    
    @IBAction func postButtonClicked(_ sender: Any) {
        let createPostVC = storyboard?.instantiateViewController(withIdentifier: "createPostVC")
        present(createPostVC!, animated: true, completion: nil)
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {

        GlobalVariable.userLocation = locations[0]
//        let latitude = GlobalVariable.userLocation?.coordinate.latitude
//        let longitude = GlobalVariable.userLocation?.coordinate.longitude
    }
    
    func getUserLocation() -> (Double, Double){
        return ((GlobalVariable.userLocation?.coordinate.latitude)!, (GlobalVariable.userLocation?.coordinate.longitude)!)
    }
    
    public var screenWidth: CGFloat {
        return UIScreen.main.bounds.width
    }
    
    // Screen height.
    public var screenHeight: CGFloat {
        return UIScreen.main.bounds.height
    }
    
    func numberOfComponents(in pickerView: UIPickerView) -> Int {
        return 2
    }
    
    func pickerView(_ pickerView: UIPickerView, numberOfRowsInComponent component: Int) -> Int {
        if component == 0 {
            return timeArray.count
        }
        
        return distanceArray.count
    }
    
    //MARK:- UIPickerViewDelegates methods
    
    func pickerView(_ pickerView: UIPickerView, titleForRow row: Int, forComponent component: Int) -> String? {
        
        if component == 0 {
            return timeArray[row]
        }
        return distanceArray[row]
    }
    func pickerView(_ pickerView: UIPickerView, didSelectRow row: Int, inComponent component: Int) {
        
        let timeSelected = timeArray[pickerView.selectedRow(inComponent: 0)]
        let distanceSelected = distanceArray[pickerView.selectedRow(inComponent: 1)]
        
        switch timeSelected {
        case "Past Week":
            timeFilter = 604800
        case "Past Month":
            timeFilter = 2592000
        case "Past 3 Months":
            timeFilter = 7776000
        case "Anytime":
            timeFilter = -1
        default:
            timeFilter = 604800
        }
        switch distanceSelected {
        case "Within 2 miles":
            distanceFilter = 3200
        case "Within 5 miles":
            distanceFilter = 8000
        case "Within 10 miles":
            distanceFilter = 16000
        case "Current Zip Code Area":
            distanceFilter = -1
        default:
            distanceFilter = 3200
        }
        getRandomPost()
    }

  
}

extension homepageVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return postsArray.count
    }
    func tableView(_ tableView: UITableView, heightForHeaderInSection section: Int) -> CGFloat {
        return 15
    }
    // Make the background color show through
    func tableView(_ tableView: UITableView, viewForHeaderInSection section: Int) -> UIView? {
        let headerView = UIView()
        headerView.backgroundColor = UIColor.clear
        return headerView
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
                                                                        //mainFeedCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "postCell") as? PostCell else
        {return  UITableViewCell() }
        let post = postsArray[indexPath.row]
        cell.configureCell(content:post.content, date: post.date)
        cell.backgroundColor = UIColor.white
        //cell.layer.borderColor = UIColor(red: 255, green: 165, blue: 0) as! CGColor
        cell.layer.borderColor = UIColor.darkGray.cgColor
        cell.layer.masksToBounds = false
        cell.layer.shadowRadius = 2.0
        cell.layer.shadowOffset = CGSize(width: -1, height: 1)
        cell.layer.borderWidth = 1
        cell.layer.cornerRadius = 8
        cell.clipsToBounds = true
        return cell
    }
    func tableView(_ tableView: UITableView, heightForRowAt indexPath: IndexPath) -> CGFloat {
        return screenHeight / 5;
    }
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        // note that indexPath.section is used rather than indexPath.row
        let vc = postDetailsVC(nibName: "postDetailsVC", bundle: nil)
        vc.postContent = "Next level blog photo booth, tousled authentic tote bag kogi"
        
        homepageVC.GlobalVariable.selectedItemId = postsArray[indexPath.row].itemId
        navigationController?.pushViewController(vc, animated: true)
    }
    override func prepare(for segue: UIStoryboardSegue, sender: Any?) {
        if segue.identifier == "postDetailSegue"
        {
            let detailViewController = ((segue.destination) as! postDetailsVC)
            let indexPath = self.mainFeedTableView!.indexPathForSelectedRow!
            let content = postsArray[indexPath.row].content
            //detailViewController.postContent = content
            detailViewController.postContent = content
        }
    }

    
}

extension UIColor {
    convenience init(red: Int, green: Int, blue: Int) {
        let newRed = CGFloat(red)/255
        let newGreen = CGFloat(green)/255
        let newBlue = CGFloat(blue)/255
        
        self.init(red: newRed, green: newGreen, blue: newBlue, alpha: 1.0)
    }
}
