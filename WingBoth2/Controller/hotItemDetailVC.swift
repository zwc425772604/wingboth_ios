//
//  hotItemDetailVC.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/25/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class hotItemDetailVC: UIViewController {
    var hotItemContent: String! = ""
    var hotItemTitle: String! = ""
    var hotItemDate: String! = ""
    var hotItemCounty: String! = ""
    var hotItemState: String! = ""
    var hotItemCountry: String! = ""
    var commentsArray = [HotItemComment]()
    
    @IBOutlet weak var contentTextView: UITextView!
    @IBOutlet weak var commentsTableView: UITableView!
    
    @IBOutlet weak var titleLabel: UILabel!
    //@IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var countyLabel: UILabel!
    @IBOutlet weak var commentTextField: UITextField!
    override func viewDidLoad() {
        super.viewDidLoad()
        view.bindToKeyboard()
       
        let tap = UITapGestureRecognizer(target: self, action: #selector(postDetailsVC.handleTap))
        view.addGestureRecognizer(tap)
        
        titleLabel.text = hotItemTitle
        let text = self.titleLabel.text
        let textRange = NSRange(location: 0, length: (text?.count)!)
        let attributedText = NSMutableAttributedString(string: text!)
        attributedText.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        self.titleLabel.attributedText = attributedText
        
        contentTextView.text = hotItemContent
        //contentLabel.text = hotItemContent
        dateLabel.text = hotItemDate
        countyLabel.text = hotItemCounty
        stateLabel.text = hotItemState
        countryLabel.text = hotItemCountry
        
        commentsTableView.delegate = self
        commentsTableView.dataSource = self
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        let itemId = hotNewsVC.GlobalVariable.selectedItemId
        hotNewsService.instance.getCommentsByHotItemId(itemId: itemId!){
            response in
            self.commentsArray = response
            self.commentsTableView.reloadData()
        }
    }

    @IBAction func backButtonPressed(_ sender: Any) {
         dismiss(animated: true, completion: nil)
    }
    
    @objc func handleTap() {
        
        view.endEditing(true)
    }
    
    @IBAction func commentButtonPressed(_ sender: Any) {
        //guard let comment = commentTextField.text else { return }
        let comment = commentTextField.text
        if comment == "" {
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1
            
            let alertController = UIAlertController(title: "Comment", message: "Comment is empty. Please enter your comment.", preferredStyle: .alert)
            alertController.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
        }else{
            
            let coor = homepageVC.GlobalVariable.userLocation
            let latitude = coor?.coordinate.latitude
            let longitude = coor?.coordinate.longitude
            let title = self.titleLabel.text
            let itemId = hotNewsVC.GlobalVariable.selectedItemId
            let itemStyle = "TreeHole"
            hotNewsService.instance.addComment(itemTitle: title!, itemId: itemId!, commentContent: comment!, latitude: latitude!, longitude: longitude!, itemStyle: itemStyle){
                returnJSON in
                guard let json = returnJSON as? [String : AnyObject] else {
                    return
                }
                let info = json["Info"] as? [String : AnyObject]
                let commentId = info!["HotCommentId"] as? String

                //add the item to allUser collection
                profileService.instance.userAddHotComment(itemTitle: title!, content: comment!, latitude: latitude!, longitude: longitude!, itemId: itemId!, commentId: commentId!, itemStyle: itemStyle){
                    returnJSON in
                    NotificationCenter.default.post(name: NSNotification.Name(rawValue: "userAddHotComment"), object: nil)
                }
            }
            self.commentTextField.text = ""
            self.commentTextField.resignFirstResponder()
        }
    }
    
}

extension hotItemDetailVC: UITableViewDelegate, UITableViewDataSource{
    func numberOfSections(in tableView: UITableView) -> Int {
        return 1
    }
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return commentsArray.count
    }
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        //mainFeedCell
        guard let cell = tableView.dequeueReusableCell(withIdentifier: "hotCommentCell") as? HotCommentCell else
        {return  UITableViewCell() }
        let comment = commentsArray[indexPath.row]
        cell.configureCell(comment: comment.comment, commentNumber: indexPath.row + 1)
        cell.setNeedsLayout() //invalidate current layout
        cell.layoutIfNeeded() //update immediately
        cell.setNeedsDisplay()
        cell.setSelected(true, animated: true)
        return cell
        
    }
    
    
    func tableView(_ tableView: UITableView, didSelectRowAt indexPath: IndexPath) {
        print("You tapped cell number \(indexPath.row).")
    }
    
    
    
}

