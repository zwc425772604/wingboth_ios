//
//  Constants.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/26/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation


typealias CompletionHandler = (_ Success: Bool) -> ()

let PROFILE_SERVICE_URL = "http://144.202.15.58:5001"
let TWEET_SERVICE_URL = "http://144.202.15.58:5000" //tweet service
let HOT_NEWS_SERVICE_URL = "http://144.202.15.58:5002"


let URL_POST_TWEET = "\(TWEET_SERVICE_URL)/item"

let URL_UESR_ALL_POSTS = "\(PROFILE_SERVICE_URL)/u/post"

let URL_ADD_USER_MONGODB = "\(PROFILE_SERVICE_URL)/u"

let URL_USER_POST = "\(PROFILE_SERVICE_URL)/u/post"
let URL_USER_COMMENT = "\(PROFILE_SERVICE_URL)/u/c"

let URL_HOMEPAGE_RANDOM_POST = "\(TWEET_SERVICE_URL)/testRandom3"

let URL_ITEM_RANDOM_HYPERLOCAL = "\(TWEET_SERVICE_URL)/item/random/hyperlocal"
let URL_ITEM_RANDOM_GLOBAL = "\(TWEET_SERVICE_URL)/item/random/global"

//profile service
let URL_USER_HOT_COMMENT = "\(PROFILE_SERVICE_URL)/u/hotcomment"
let URL_GET_UPDATE = "\(PROFILE_SERVICE_URL)/update"

//tweet service
let URL_ITEM = "\(TWEET_SERVICE_URL)/item/"


//hot news service
let URL_HOT_ITEM_POST = "\(HOT_NEWS_SERVICE_URL)/hot/treehole"
let URL_HOT_ITEM_RANDOM = "\(HOT_NEWS_SERVICE_URL)/hot/random"
let URL_HOT = "\(HOT_NEWS_SERVICE_URL)/hot/"

/*
   Profile Service
    /u       POST create user
    /u       GET  get user info
    /u/c     GET  get user all comments
    /u/post  GET  get user all posts
    /u/c/<commentID>  POST add comment to allUser's allComments array
    /u/post/<itemId>  POST add post to allUser's allPosts array
 
 
     Tweet Service
    /item POST      post a new item
    /item/<itemId> GET
    /item/<itemId>/c  POST  add comment
    /item/<itemId>/c  GET  get post's comment
 
 */
