//
//  AppDelegate.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit
import Firebase
import SwiftKeychainWrapper
import GoogleSignIn
import Alamofire
import MapKit
import CoreLocation

@UIApplicationMain
class AppDelegate: UIResponder, UIApplicationDelegate, GIDSignInDelegate,CLLocationManagerDelegate{

    var window: UIWindow?

    let locationManager = CLLocationManager()
    
    func application(_ application: UIApplication, didFinishLaunchingWithOptions launchOptions: [UIApplicationLaunchOptionsKey: Any]?) -> Bool {
        
        // Override point for customization after application launch.
       
        FirebaseApp.configure()
        
        GIDSignIn.sharedInstance().clientID = FirebaseApp.app()?.options.clientID
        GIDSignIn.sharedInstance().delegate = self
        
        
        let status  = CLLocationManager.authorizationStatus()
        
        // 2
        if status == .notDetermined {
            print("not determine")
            locationManager.requestWhenInUseAuthorization()
            exit(0)
        }
        // 3
        if status == .denied || status == .restricted {
            
            let alertWindow = UIWindow(frame: UIScreen.main.bounds)
            alertWindow.rootViewController = UIViewController()
            alertWindow.windowLevel = UIWindowLevelAlert + 1
            
            let alertController = UIAlertController(title: "Location Services Disabled", message: "Please enable Location Services in Settings.", preferredStyle: .alert)
            let alertActionOkay = UIAlertAction(title: "Ok", style: .default) {
                (_) in
                exit(0)
            }
            alertController.addAction(alertActionOkay)
            
            alertWindow.makeKeyAndVisible()
            alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            return false
        }
        
        // 4
        locationManager.delegate = self
        locationManager.startUpdatingLocation()
     
        
        checkForUpdate() {
            response in
            guard let json = response as? [String : AnyObject] else {
                return
            }
            let version = json["Message"] as? String
            if version != "1.0"{
                let alertWindow = UIWindow(frame: UIScreen.main.bounds)
                alertWindow.rootViewController = UIViewController()
                alertWindow.windowLevel = UIWindowLevelAlert + 1
                
                let alertController = UIAlertController(title: "New Version Available", message: "Please go to the App Store to update the app.", preferredStyle: .alert)
                let alertActionOkay = UIAlertAction(title: "Ok", style: .default) {
                    (_) in
                    exit(0)
                }
                alertController.addAction(alertActionOkay)
                
                alertWindow.makeKeyAndVisible()
                alertWindow.rootViewController?.present(alertController, animated: true, completion: nil)
            }
            
            
            let currentUser = Auth.auth().currentUser
            if currentUser != nil && (currentUser?.isEmailVerified)!
            {
                print("existed current user")
                let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
                let homePage = mainStoryboard.instantiateViewController(withIdentifier: "homeVC")
                self.window?.rootViewController = homePage
            }
            
        }
     
       
        

        let navigationBarAppearace = UINavigationBar.appearance()
        navigationBarAppearace.tintColor = .white
        navigationBarAppearace.barTintColor = UIColor.black
        UINavigationBar.appearance().isTranslucent = false
        // change navigation item title color
        navigationBarAppearace.titleTextAttributes = [NSAttributedStringKey.foregroundColor: UIColor.white]
                let statusBar: UIView = UIApplication.shared.value(forKey: "statusBar") as! UIView
                if statusBar.responds(to:#selector(setter: UIView.backgroundColor)) {
                    statusBar.backgroundColor = UIColor.black
                }
        UIApplication.shared.statusBarStyle = UIStatusBarStyle.lightContent
        
        return true
    }
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        let currentLocation = locations.last!
        print("Current location: \(currentLocation)")
        homepageVC.GlobalVariable.userLocation = locations[0]
    }

    func checkForUpdate(completeonClosure: @escaping (AnyObject?) -> ()) {
        let header = [
            "Content-Type": "application/json; charset=utf-8"
        ]
            Alamofire.request(URL_GET_UPDATE, headers: header)
                .responseJSON {
                    response in
                    completeonClosure(response.result.value as AnyObject)
            }
    }
    
    
    @available(iOS 9.0, *)
    func application(_ application: UIApplication, open url: URL, options: [UIApplicationOpenURLOptionsKey : Any])
        -> Bool {
            return GIDSignIn.sharedInstance().handle(url,
                                                     sourceApplication:options[UIApplicationOpenURLOptionsKey.sourceApplication] as? String,
                                                     annotation: [:])
    }
    func application(_ application: UIApplication, open url: URL, sourceApplication: String?, annotation: Any) -> Bool {
        return GIDSignIn.sharedInstance().handle(url,
                                                 sourceApplication: sourceApplication,
                                                 annotation: annotation)
    }

    
    func sign(_ signIn: GIDSignIn!, didSignInFor user: GIDGoogleUser!, withError error: Error?) {
        // ...
        if let error = error {
            // ...
            return
        }
        
        guard let authentication = user.authentication else { return }
        let credential = GoogleAuthProvider.credential(withIDToken: authentication.idToken,
                                 accessToken: authentication.accessToken)
        let userEmail = user.profile.email
        Auth.auth().fetchProviders(forEmail: userEmail!, completion: { (providers, error) in
            if let error = error {
                print(error.localizedDescription)
            } else if let providers = providers {
                print(providers)
                
            }
        })
        //print("\(credential.provider), \(user.userID), \(userEmail)")
        // ...
        /*
        In the signIn:didSignInForUser:withError: method, get a Google ID token and Google access token from the GIDAuthentication object and exchange them for a Firebase credential:*/
        Auth.auth().signInAndRetrieveData(with: credential) { (authResult, error) in
            if let error = error {
                // ...
                return
            }
            // User is signed in
            // ...
            
            print("login user id is \(String(describing: authResult?.user.uid))")
            let mainStoryboard:UIStoryboard = UIStoryboard(name: "Main", bundle: nil)
            let homePage = mainStoryboard.instantiateViewController(withIdentifier: "homeVC")
            self.window?.rootViewController = homePage
        }
    }
    
    func sign(_ signIn: GIDSignIn!, didDisconnectWith user: GIDGoogleUser!, withError error: Error!) {
        // Perform any operations when the user disconnects from app here.
        // ...
    }

    func applicationWillResignActive(_ application: UIApplication) {
        // Sent when the application is about to move from active to inactive state. This can occur for certain types of temporary interruptions (such as an incoming phone call or SMS message) or when the user quits the application and it begins the transition to the background state.
        // Use this method to pause ongoing tasks, disable timers, and invalidate graphics rendering callbacks. Games should use this method to pause the game.
    }

    func applicationDidEnterBackground(_ application: UIApplication) {
        // Use this method to release shared resources, save user data, invalidate timers, and store enough application state information to restore your application to its current state in case it is terminated later.
        // If your application supports background execution, this method is called instead of applicationWillTerminate: when the user quits.
    }

    func applicationWillEnterForeground(_ application: UIApplication) {
        // Called as part of the transition from the background to the active state; here you can undo many of the changes made on entering the background.
    }

    func applicationDidBecomeActive(_ application: UIApplication) {
        // Restart any tasks that were paused (or not yet started) while the application was inactive. If the application was previously in the background, optionally refresh the user interface.
    }

    func applicationWillTerminate(_ application: UIApplication) {
        // Called when the application is about to terminate. Save data if appropriate. See also applicationDidEnterBackground:.
    }


}

