//
//  Post.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/31/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation

struct Post {
    private var _title: String
    private var _content: String
    private var _date: String
    private var _itemId: String
    var title : String {
        return _title
    }
    var content: String{
        return _content
    }
    var date: String{
        return _date
    }
    var itemId: String{
        return _itemId
    }
    init(title: String, content: String, date: String, itemId : String){
        self._title = title
        self._content = content
        self._date = date
        self._itemId = itemId
    }
}
