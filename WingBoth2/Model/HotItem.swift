//
//  HotItem.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation


struct HotItem {
    private var _title: String
    private var _content: String
    private var _date: String
    private var _itemId: String
    private var _county: String
    private var _state: String
    private var _country: String
    
    var title : String {
        return _title
    }
    var content: String{
        return _content
    }
    var date: String{
        return _date
    }
    var itemId: String{
        return _itemId
    }
    var country: String {
        return _country
    }
    var county: String{
        return _county
    }
    var state: String{
        return _state
    }
    init(title: String, content: String, county: String, state: String, country:String, date: String, itemId : String){
        self._title = title
        self._content = content
        self._date = date
        self._itemId = itemId
        self._county = county
        self._country = country
        self._state = state
    }
}
