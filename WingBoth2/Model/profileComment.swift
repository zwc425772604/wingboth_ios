//
//  profileComment.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/15/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation

struct profileComment {
    private var _postContent: String
    private var _postId: String
    private var _comment: String
    private var _date: String
    
    var postContent: String{
        return _postContent
    }
    var postId: String{
        return _postId
    }
    var comment: String{
        return _comment
    }
    var date: String{
        return _date
    }
    init(postContent: String, postId: String,comment: String, date: String){
        self._postContent = postContent
        self._postId = postId
        self._comment = comment
        self._date = date
    }
}
