//
//  Comment.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/9/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation

struct Comment {
    private var _comment: String
    private var _date: String
    
    var comment: String{
        return _comment
    }
    var date: String{
        return _date
    }
    init(comment: String, date: String){
        self._comment = comment
        self._date = date
    }
}
