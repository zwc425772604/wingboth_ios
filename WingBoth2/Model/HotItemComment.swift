//
//  HotItemComment.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/26/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import Foundation

class HotItemComment{
    
    private var _itemTitle: String
    private var _comment: String
    private var _date: String
    private var _itemId : String
    var itemTitle: String{
        return _itemTitle
    }
    var comment: String{
        return _comment
    }
    var date: String{
        return _date
    }
    var itemId : String{
        return _itemId
    }
    init(itemTitle: String, comment: String, date: String, itemId: String){
        self._itemTitle = itemTitle
        self._comment = comment
        self._date = date
        self._itemId = itemId
    }
    
}
