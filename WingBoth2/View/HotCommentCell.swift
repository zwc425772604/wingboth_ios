//
//  HotCommentCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/26/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class HotCommentCell: UITableViewCell {

    @IBOutlet weak var commentCountLabel: UILabel!
    @IBOutlet weak var commentContentLabel: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(comment: String, commentNumber : Int){
        self.commentContentLabel.text = comment
        self.commentCountLabel.text = "#" + String(commentNumber)
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
