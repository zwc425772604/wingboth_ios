//
//  profileHotCommentCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/29/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class profileHotCommentCell: UITableViewCell {

    @IBOutlet weak var itemLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }

    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell( itemTitle :String, comment: String, date: String){
        self.itemLabel.text = itemTitle
        self.commentLabel.text = comment
        self.dateLabel.text = date
    }
}
