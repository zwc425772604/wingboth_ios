//
//  mainFeedCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/6/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class mainFeedCell: UITableViewCell {

   

    
    @IBOutlet weak var tLabel: UILabel!
    @IBOutlet weak var cLabel: UILabel!
    @IBOutlet weak var dLabel: UILabel!
 
    func configureCell(title:String, content: String, date: String){
        self.tLabel.text = title
        self.cLabel.text = content
        self.dLabel.text = date
    }

}
