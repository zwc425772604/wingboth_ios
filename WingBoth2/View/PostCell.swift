//
//  PostCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 7/31/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class PostCell: UITableViewCell {
 

    @IBOutlet weak var contentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!

    func configureCell(content: String, date: String){
        self.contentLabel.text = content
        self.dateLabel.text = date

    }
}
