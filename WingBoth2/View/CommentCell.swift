//
//  CommentCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/9/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class CommentCell: UITableViewCell {

  

    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var commentNumberLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell(comment: String, commentNumber : Int){
        self.commentLabel.text = comment
        self.commentNumberLabel.text = "#" + String(commentNumber)
    }

}
