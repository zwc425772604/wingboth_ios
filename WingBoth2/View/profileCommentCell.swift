//
//  profileCommentCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/15/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class profileCommentCell: UITableViewCell {

    @IBOutlet weak var postContentLabel: UILabel!
    @IBOutlet weak var commentLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }
    
    func configureCell( postContent :String, comment: String, date: String){
        self.postContentLabel.text = postContent
        self.commentLabel.text = comment
        self.dateLabel.text = date
    }
}
