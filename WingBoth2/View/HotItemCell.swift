//
//  HotItemCell.swift
//  WingBoth2
//
//  Created by Weichao Zhao on 8/24/18.
//  Copyright © 2018 Weichao Zhao. All rights reserved.
//

import UIKit

class HotItemCell: UITableViewCell {

    @IBOutlet weak var titleLabel: UILabel!
    @IBOutlet weak var dateLabel: UILabel!
    
    @IBOutlet weak var countryLabel: UILabel!
    @IBOutlet weak var stateLabel: UILabel!
    @IBOutlet weak var countyLabel: UILabel!
    
    override func awakeFromNib() {
        super.awakeFromNib()
    }
    
    func configureCell(title: String, county: String, state : String, country: String, date: String){
        self.titleLabel.text = title
        self.dateLabel.text = date
        self.countyLabel.text = county
        self.stateLabel.text = state
        self.countryLabel.text = country
        
        let text = self.titleLabel.text
        let textRange = NSRange(location: 0, length: (text?.count)!)
        let attributedText = NSMutableAttributedString(string: text!)
        attributedText.addAttribute(NSAttributedStringKey.underlineStyle, value: NSUnderlineStyle.styleSingle.rawValue, range: textRange)
        self.titleLabel.attributedText = attributedText
    }
    
    override func setSelected(_ selected: Bool, animated: Bool) {
        super.setSelected(selected, animated: animated)
    }

}
